package com.azimlord.whatsnew.model

import androidx.annotation.DrawableRes

open class WhatsNewItem {
    data class Title(@DrawableRes val icon: Int, val title: String)
    data class Content(val message: String)
}