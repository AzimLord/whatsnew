package com.azimlord.whatsnew.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.RecyclerView
import com.azimlord.whatsnew.R
import com.azimlord.whatsnew.model.WhatsNewItem
import kotlinx.android.synthetic.main.item_whats_new_content.view.*
import kotlinx.android.synthetic.main.item_whats_new_title.view.*

class WhatsNewAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val whatsNewItems = ArrayList<Any>()

    companion object {
        private const val VIEW_TYPE_TITLE = 1
        private const val VIEW_TYPE_CONTENT = 2
    }

    fun addTitle(@DrawableRes icon: Int, title: String) {
        this.whatsNewItems.add(WhatsNewItem.Title(icon, title))
        notifyItemChanged(this.whatsNewItems.size - 1)
    }

    fun addMessage(sectionContent: String) {
        this.whatsNewItems.add(WhatsNewItem.Content(sectionContent))
        notifyItemChanged(this.whatsNewItems.size - 1)
    }

    override fun getItemViewType(position: Int): Int {
        return when (whatsNewItems[position]) {
            is WhatsNewItem.Title -> {
                VIEW_TYPE_TITLE
            }
            is WhatsNewItem.Content -> {
                VIEW_TYPE_CONTENT
            }
            else -> {
                throw Exception("No matching views for WhatsNewAdapter")
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_TITLE -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_whats_new_title, parent, false)
                TitleViewHolder(view)
            }
            VIEW_TYPE_CONTENT -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_whats_new_content, parent, false)
                ContentViewHolder(view)
            }
            else -> {
                throw Exception("No matching viewType for WhatsNewAdapter")
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = whatsNewItems[position]
        when (holder) {
            is TitleViewHolder -> {
                holder.bind(item as WhatsNewItem.Title)
            }
            is ContentViewHolder -> {
                holder.bind(item as WhatsNewItem.Content)
            }
        }
    }

    override fun getItemCount(): Int {
        return whatsNewItems.size
    }

    class TitleViewHolder(private val view: View): RecyclerView.ViewHolder(view) {
        fun bind(item: WhatsNewItem.Title) {
            view.iv_title.setImageResource(item.icon)
            view.tv_title.text = item.title
        }
    }

    class ContentViewHolder(private val view: View): RecyclerView.ViewHolder(view) {
        fun bind(item: WhatsNewItem.Content) {
            view.tv_content.text = item.message
        }
    }

}