package com.azimlord.whatsnew.view

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.azimlord.whatsnew.R
import kotlinx.android.synthetic.main.activity_whats_new.*

class WhatsNewActivity : AppCompatActivity() {

    private lateinit var adapter: WhatsNewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_whats_new)

        setSupportActionBar(v_toolbar)

        adapter = WhatsNewAdapter()
        rv_whats_new.adapter = adapter

        var isVideo = true

        if (isVideo) {
            v_video.visibility = View.VISIBLE
        } else {
            v_video.visibility = View.GONE
        }

        adapter.addTitle(R.drawable.ic_add, "New")
        adapter.addMessage("Share songs & setlists from the library!")
        adapter.addMessage("Also share articles & videos from Learn")
        adapter.addMessage("Rate content in Learn and share feedback")

        adapter.addTitle(R.drawable.ic_updated, "Updated")
        adapter.addMessage("Increased thumbnail sizes in Learn")
        adapter.addMessage("Instructors now have website links")
        adapter.addMessage("Updated privacy policy menu")
        adapter.addMessage("Updated \"about\" menu")
        adapter.addMessage("Increased shop images aspect ratio")

        adapter.addTitle(R.drawable.ic_fixed, "Fixed")
        adapter.addMessage("Crash when scrolling in Plus Deals")
        adapter.addMessage("Crash when creating a new song")
        adapter.addMessage("Display bug in the discount title")
        adapter.addMessage("Placeholder 1")
        adapter.addMessage("Placeholder 2")

        v_scroll.post {
            v_content.post {
                if (v_content.height <= v_scroll.height) {
                    v_gradient.visibility = View.GONE
                }
            }
        }

        btn_open_google_play.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW).apply {
                data = Uri.parse(
                    "https://play.google.com/store/apps/details?id=com.soundbrenner.pulse")
            }
            startActivity(intent)
        }
    }
}